#include "manager.hpp"

char gato[4][3] = {
    {1, 1, 1},
    {0, 0, 0},
    {2, 2, 2},
    {3, 3, 3},
};

void setup() {
    Serial.begin(115200);
    Serial.println("Starting 2.0");
    init_cuisinier();
    Serial.println("Cuisinie initialise");
    delay(6000);
    // test_motor();
    test_any_io();
    // back_and_forward();
    //first_move();

    char output = 0;

    while(output != -1){
        output = fait_un_gato(gato);
        if(output != -1){
            gato[output][0] = 0;
            gato[output][1] = 0;
            gato[output][2] = 0;
            delay(3000);
        }
    }

    Serial.print("Plus de gâteau disponible\n");
    while(1);

}

void loop() {}
