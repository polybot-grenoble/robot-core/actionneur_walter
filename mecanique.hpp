#ifndef MECANIQUE
#define MECANIQUE

#include <Servo.h>

#include <vector>

#include "Arduino.h"
#include "unit.hpp"

using namespace std;

typedef enum { MOVE = 140, UP = 45, MIDDLE = 30, DOWN = 8 } TranslatorPosition;
class Rotor;
class Translator {
   private:
    Servo _servo;
    TranslatorPosition _current_position;
    int _previous_angle, _next_angle;
    micro_second _start_time_order, _duration_order;

    int _current_angle();

    int _protected_write(int angle);

   public:
    Translator(const pin pin_servo,
               const TranslatorPosition first_position = MOVE);

    micro_second go_to(const TranslatorPosition position);

    bool reached_desired_position();
    bool can_move;
};

class Rotor {
   private:
    const pin _pos_pin, _neg_pin;
    const vector<pin> _pin_capteurs;
    int _position, _desired_position;  // capteur index * 2 (+1 if in between)
    bool _reached;
    void print_state();

   public:
    Rotor(pin pos_pin, pin neg_pin, vector<pin> pin_capteurs);

    void locate_rotor();

    void go_to(unsigned int capteur_index);

    void update();
    void stop();

    bool reached_desired_position();

    void handle_capt_interrupt(unsigned int capteur_index);
    bool can_rotate;
};

#endif
