#include "mecanique.hpp"

#include "Arduino.h"

#define min_servo_angle 5    // Bas
#define max_servo_angle 140  // Haut
#define us_per_deg 2500

int Translator::_current_angle() {
    int mapped_angle =
        map(micros(), _start_time_order, _start_time_order + _duration_order,
            _previous_angle, _next_angle);
    return constrain(mapped_angle, min(_previous_angle, _next_angle),
                     max(_previous_angle, _next_angle));
}

int Translator::_protected_write(int angle) {
    if (angle < min_servo_angle) {
        Serial.print("[Erreur] Angle demandé pour le servo trop petit, ");
        Serial.print(angle);
        Serial.print(" remplacé par ");
        angle = min_servo_angle;
        Serial.println(angle);
    }
    if (angle > max_servo_angle) {
        Serial.print("[Erreur] Angle demandé pour le servo trop grand, ");
        Serial.print(angle);
        Serial.print(" remplacé par ");
        angle = max_servo_angle;
        Serial.println(angle);
    }
    _servo.write(angle);

    return angle;
}

Translator::Translator(const pin pin_servo,
                       const TranslatorPosition first_position) {
    _servo.attach(pin_servo);

    _previous_angle = _next_angle = first_position;
    _start_time_order = micros();
    _duration_order = 0;
    go_to(first_position);
    delayMicroseconds(us_per_deg * 180);
}

micro_second Translator::go_to(const TranslatorPosition position) {
    int angle = _protected_write(position);

    int current_angle = _current_angle();

    _duration_order = us_per_deg * abs(angle - current_angle);
    _start_time_order = micros();

    _previous_angle = current_angle;
    _next_angle = angle;

    return _duration_order;
}

bool Translator::reached_desired_position() {
    return micros() >= (_start_time_order + _duration_order);
}


Rotor::Rotor(pin pos_pin, pin neg_pin, std::vector<pin> pin_capteurs)
    : _pos_pin(pos_pin), _neg_pin(neg_pin), _pin_capteurs(pin_capteurs) {
    pinMode(_pos_pin, OUTPUT);
    pinMode(_neg_pin, OUTPUT);
    for (pin pin_capt : _pin_capteurs) pinMode(pin_capt, INPUT_PULLDOWN);
    _position = -1;
    _desired_position = _pin_capteurs.size() / 2;
    _reached = false;
    can_rotate = true;
}

void Rotor::locate_rotor() {
    // From L298N L9110 datasheet, motor will not be connected
    digitalWrite(_pos_pin, HIGH);
    digitalWrite(_neg_pin, HIGH);

    _position = -1;
    for (int i = 0; i < _pin_capteurs.size(); i++) {
        if (digitalRead(_pin_capteurs[i])) {
            _position = 2 * i;
            Serial.print("Located at ");
            Serial.println(i);
            break;
        }
    }

    digitalWrite(_pos_pin, LOW);
    digitalWrite(_neg_pin, _position < 0);
}

void Rotor::go_to(unsigned int capteur_index) {
    _desired_position = 2 * capteur_index;
    update();
}

void Rotor::print_state() {
    Serial.print("desired = ");
    Serial.print(_desired_position);
    Serial.print(", position = ");
    Serial.print(_position);
    Serial.print(", reached = ");
    Serial.print(_reached);
    Serial.print(", pos = ");
    Serial.print(digitalRead(_pos_pin));
    Serial.print(", neg = ");
    Serial.println(digitalRead(_neg_pin));
}

void Rotor::update() {
    if(can_rotate){
        digitalWrite(_pos_pin, _position < _desired_position);
        digitalWrite(_neg_pin, _position > _desired_position);
    }
    _reached = _desired_position == _position;

    // print_state();
};

void Rotor::stop(){
    digitalWrite(_pos_pin, LOW);
    digitalWrite(_neg_pin, LOW);
}

bool Rotor::reached_desired_position() { return _reached; }

void Rotor::handle_capt_interrupt(unsigned int capteur_index) {
    _position = 2 * capteur_index;
    if (!digitalRead(_pin_capteurs[capteur_index])) {
        _position += digitalRead(_pos_pin);
        _position -= digitalRead(_neg_pin);
    }
    Serial.print("Interruption capteur ");
    Serial.print(capteur_index);
    Serial.print(digitalRead(_pin_capteurs[capteur_index]));
    Serial.println(digitalRead(_pin_capteurs[capteur_index]) ? " entrant"
                                                             : " sortant");
    print_state();
    update();
    print_state();
    Serial.println("");
};

