#include <vector>
#include "manager.hpp"



#define pin_P1 PA13
#define pin_P2 PH1
#define pin_P3 PC3
#define pin_P4 PA0
const pin pin_capteurs[4] = {pin_P1, pin_P2, pin_P3, pin_P4};

#define pin_servo PA15
#define pin_pump PC2
#define pin_motor_pos PB7
#define pin_motor_neg PA14


Rotor* rotor;
Translator* translator;

micro_second time_since_last_interrupt;

template <unsigned int capteur_index>
void handle_capt_interrupt() {
    micro_second now = micros();
    if (now - time_since_last_interrupt > 50000) {
        time_since_last_interrupt = now;
        rotor->handle_capt_interrupt(capteur_index);
    }
}

template <unsigned int capteur_index>
void attach_capt_interrupt() {
    attachInterrupt(digitalPinToInterrupt(pin_capteurs[capteur_index]),
                    handle_capt_interrupt<capteur_index>, CHANGE);
}

void init_cuisinier() {

    pinMode(pin_pump, OUTPUT);

    translator = new Translator(pin_servo, MOVE);
    rotor = new Rotor(
        pin_motor_pos, pin_motor_neg,
        std::vector<pin>(std::begin(pin_capteurs), std::end(pin_capteurs)));
    time_since_last_interrupt = micros();
    // attach_capt_interrupt<0>();
    // attach_capt_interrupt<1>();
    // attach_capt_interrupt<2>();
    // attach_capt_interrupt<3>();
    // rotor->locate_rotor();
}

bool is_feasible_recipe() {
    // TODO
    return true;
}
micro_second time_to_cook() {
    // TODO
    return 0;
}
void start_cooking() {
    // TODO
}
void aboard_cooking() {
    // TODO
}

void update() { rotor->update(); }

void test_motor() {
    Serial.println("test_motor");
    delay(500);
    int step = 0, last_step = -1;

    while (1) {
        if (step != last_step) {
            Serial.print("Step : ");
            Serial.println(step);
            last_step = step;
        }
        switch (step) {
            case 0:
                delayMicroseconds(translator->go_to(MOVE));
                rotor->go_to(0);
                step++;
                break;
            case 1:
                if (rotor->reached_desired_position()) {
                    translator->go_to(UP);
                    step++;
                }
                break;
            case 2:
                if (translator->reached_desired_position()) {
                    delay(500);
                    translator->go_to(MIDDLE);
                    step++;
                }
                break;
            case 3:
                if (translator->reached_desired_position()) {
                    delay(500);
                    translator->go_to(DOWN);
                    step++;
                }
                break;
            case 4:
                if (translator->reached_desired_position()) {
                    delay(500);
                    delayMicroseconds(translator->go_to(MOVE));
                    rotor->go_to(3);
                    step++;
                }
                break;
            case 5:
                if (rotor->reached_desired_position()) {
                    translator->go_to(DOWN);
                    step++;
                }
                break;
            case 6:
                if (translator->reached_desired_position()) {
                    translator->go_to(MOVE);
                    step++;
                }
                break;
            case 7:
                if (translator->reached_desired_position()) {
                    rotor->go_to(3);
                    step++;
                }
                break;
            case 8:
                if (rotor->reached_desired_position()) {
                    delay(500);
                    rotor->go_to(2);
                    step++;
                }
                break;
            case 9:
                if (rotor->reached_desired_position()) {
                    delay(500);
                    rotor->go_to(1);
                    step++;
                }
                break;
            case 10:
                if (rotor->reached_desired_position()) {
                    delay(500);
                    rotor->go_to(0);
                    step++;
                }
                break;
            case 11:
                if (rotor->reached_desired_position()) {
                    step = 0;
                }
                break;

            default:
                return;
        }
        update();
    }
}

char safe_serial_read() {
    while (!Serial.available())
        ;
    return Serial.read();
}

void test_any_io() {
    Serial.println(
        "Pour tester une entree sortie saisissez une commande parmis les "
        "suivantes :");
    Serial.println(">>> motor_pos");
    Serial.println(
        "    pour faire tourner le moteur dans le sens positif pendant 10 "
        "secondes");
    Serial.println(">>> motor_neg");
    Serial.println(
        "    pour faire tourner le moteur dans le sens negatif pendant 10 "
        "secondes");
    Serial.println(">>> capteurs");
    Serial.println(
        "    pour afficher l'etat des capteurs en alumant le moteur dans les "
        "deux directions");
    Serial.println(">>> capteurs_off");
    Serial.println("    pour afficher l'etat des capteurs");
    // Serial.println(">>> servo x");
    // Serial.println("    pour mettre le servo à l'angle x");
    Serial.println(">>> servo_position x");
    Serial.println("    pour mettre le servo à la x-ième position [0-3]");
    Serial.println(">>> pump");
    Serial.println("    pour allumer la pompe 5 secondes");

    char current_char;

    while (true) {
        Serial.println("Attente de la commande:");
        String command_name = "";
        for (current_char = safe_serial_read();
             current_char != ' ' && current_char != '\n';
             current_char = safe_serial_read())
            command_name += current_char;

        Serial.print("Commande reçu: ");
        Serial.println(command_name);

        if (command_name == "motor_pos") {
            digitalWrite(pin_motor_pos, HIGH);
            digitalWrite(pin_motor_neg, LOW);
            delay(5000);
            digitalWrite(pin_motor_pos, LOW);
            digitalWrite(pin_motor_neg, LOW);
        } else if (command_name == "motor_neg") {
            digitalWrite(pin_motor_pos, LOW);
            digitalWrite(pin_motor_neg, HIGH);
            delay(5000);
            digitalWrite(pin_motor_pos, LOW);
            digitalWrite(pin_motor_neg, LOW);
        } else if (command_name == "pump") {
            digitalWrite(pin_pump, HIGH);
            delay(5000);
            digitalWrite(pin_pump, LOW);
        } else if (command_name == "capteurs") {
            digitalWrite(pin_motor_pos, HIGH);
            digitalWrite(pin_motor_neg, HIGH);

            Serial.print("Capteurs : ");

            for (int i = 0; i < 4; i++)
                Serial.print(digitalRead(pin_capteurs[i]));
            Serial.println("");

            digitalWrite(pin_motor_pos, LOW);
            digitalWrite(pin_motor_neg, LOW);
        } else if (command_name == "capteurs_off") {
            Serial.print("Capteurs : ");

            for (int i = 0; i < 4; i++)
                Serial.print(digitalRead(pin_capteurs[i]));
            Serial.println("");
        } else if (command_name == "servo_position") {
            int pos = safe_serial_read() - '0';
            safe_serial_read();

            switch (pos) {
                case 0:
                    translator->go_to(MOVE);
                    break;
                case 1:
                    translator->go_to(UP);
                    break;
                case 2:
                    translator->go_to(MIDDLE);
                    break;
                case 3:
                    translator->go_to(DOWN);
                    break;
            }
        } else {
            Serial.print("Unknown command : ");
            Serial.println(command_name);
        }
        delay(200);
    }
    Serial.println("Fin de la commande");
}

void back_and_forward() {
    noInterrupts();
    while (1) {
        for (int i = 0; i < 2; i++) {
            digitalWrite(pin_motor_pos, i);
            digitalWrite(pin_motor_neg, !i);
            delay(4000);
        }
        
        for (int i = 0; i < 2; i++) {
            digitalWrite(pin_pump, i);
            delay(4000);
        }

    }
}


void first_move(){

    // while(1){
    //     rotor->go_to(0);
    //     while(!rotor->reached_desired_position());
    //     rotor->go_to(1);
    //     while(!rotor->reached_desired_position());
    //     rotor->go_to(2);
    //     while(!rotor->reached_desired_position());
    //     rotor->go_to(3);
    //     while(!rotor->reached_desired_position());
    //     rotor->go_to(2);
    //     while(!rotor->reached_desired_position());
    //     rotor->go_to(1);
    //     while(!rotor->reached_desired_position());

    // };



    rotor->go_to(2);

    while(!rotor->reached_desired_position());

    while(1){

        delayMicroseconds(translator->go_to(DOWN));
        digitalWrite(pin_pump, HIGH);
        delay(1000);
        delayMicroseconds(translator->go_to(MOVE));

        rotor->go_to(3);
        while(!rotor->reached_desired_position());

        delayMicroseconds(translator->go_to(DOWN));
        digitalWrite(pin_pump, LOW);
        delay(1000);
        delayMicroseconds(translator->go_to(MOVE));

        delay(2000);






        delayMicroseconds(translator->go_to(DOWN));
        digitalWrite(pin_pump, HIGH);
        delay(1000);
        delayMicroseconds(translator->go_to(MOVE));

        
        rotor->go_to(2);
        while(!rotor->reached_desired_position());

        
        delayMicroseconds(translator->go_to(DOWN));
        digitalWrite(pin_pump, LOW);
        delay(1000);
        delayMicroseconds(translator->go_to(MOVE));

        delay(2000);

    }

}



char get_first_in_pile(char pile[3]){
    char c;
    for (c = 2; c >=0; c--){
        if(pile[c] != 0) return c;
    }
    return -1;

}

bool is_pile_empty(char pile[3]){
    return pile[0] == 0;
};



char locate_empty_pile(char piles[4][3]){
    char p;
    for (p = 0; p < 4; p++){
        if(is_pile_empty(piles[p])){
            Serial.print("Empty pile found :");
            Serial.println((int)p);
            return p;
        }
    }
    return -1;
}

char locate_piece(char piles[4][3], char piece){
    char p;
    char c;
    for (p = 0; p < 4; p++){

        c = get_first_in_pile(piles[p]);

        if(piles[p][c] == piece){


            return p<<4 | c;
        };
    }
    return -1;
}

char fait_un_gato(char piles[4][3]){


    char c;
    char output_pile = locate_empty_pile(piles);
    if(output_pile == -1) return -1;

    char pos_gato[3];                       // 4LSB : hauteur,  4MSB : pile
    pos_gato[0] = locate_piece(piles, 1);
    if(pos_gato[0] == -1) return -1;
    pos_gato[1] = locate_piece(piles, 2);
    if(pos_gato[1] == -1) return -1;
    pos_gato[2] = locate_piece(piles, 3);
    if(pos_gato[2] == -1) return -1;

    Serial.print("Piece marron : ");
    Serial.print(pos_gato[0], 16);
    Serial.println();

    Serial.print("Piece jaune : ");
    Serial.print(pos_gato[1], 16);
    Serial.println();

    Serial.print("Piece rose : ");
    Serial.print(pos_gato[2], 16);
    Serial.println();

    rotor->can_rotate = false;

    for(c = 0; c < 3; c++){
        delayMicroseconds(translator->go_to(MOVE));
        delay(1000);



        Serial.print("Goto : ");
        Serial.println((int)(pos_gato[c]>>4));

        rotor->can_rotate = true;
        rotor->go_to(pos_gato[c]>>4);
        while(!rotor->reached_desired_position());
        delay(1000);
        rotor->can_rotate = false;
        rotor->stop();


        switch(pos_gato[c] & 0xF){
            case 0:
                delayMicroseconds(translator->go_to(DOWN));
                break;
            case 1:
                delayMicroseconds(translator->go_to(MIDDLE));
                break;
            case 2:
                delayMicroseconds(translator->go_to(UP));
                break;
        }
        delay(1000);

        digitalWrite(pin_pump, HIGH);
        
        delay(1000);
        delayMicroseconds(translator->go_to(MOVE));
        delay(1000);

        
        Serial.print("Goto : ");
        Serial.println((int)output_pile);



        rotor->can_rotate = true;
        rotor->go_to(output_pile);
        while(!rotor->reached_desired_position());
        delay(1000);
        rotor->can_rotate = false;
        rotor->stop();

        switch(c){
            case 0:
                delayMicroseconds(translator->go_to(DOWN));
                break;
            case 1:
                delayMicroseconds(translator->go_to(MIDDLE));
                break;
            case 2:
                delayMicroseconds(translator->go_to(UP));
                break;
        }
        delay(1000);
        digitalWrite(pin_pump, LOW);
        delay(1000);
    }


    return output_pile;

}