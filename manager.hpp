#ifndef __MANAGER__
#define __MANAGER__

#include "unit.hpp"
#include "mecanique.hpp"


void init_cuisinier();

bool is_feasible_recipe();
micro_second time_to_cook();
void start_cooking();
void aboard_cooking();

void update();

void test_motor();

void test_any_io();

void back_and_forward();

void first_move();
char get_first_in_pile(char pile[3]);
bool is_pile_empty(char pile[3]);
char locate_empty_pile(char piles[4][3]);
char locate_piece(char piles[4][3], char piece);
char fait_un_gato(char piles[4][3]);

#endif
